﻿using System;

using GraphQL.Types;

using Microsoft.Extensions.DependencyInjection;

using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public class CustomerSchema : Schema
    {
        public CustomerSchema(IServiceProvider resolver) : base(resolver)
        {

            Query = resolver.GetService<CustomerQuery>();

        }
    }
}
