﻿using System.Collections.Generic;

using GraphQL.Types;

using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public class CustomerQuery : ObjectGraphType
    {
        public CustomerQuery(IRepository<Customer> customerRepository)
        {
            Field<ListGraphType<CustomerType>>("customers")
                .Resolve(context => customerRepository.GetAllAsync());
        }

    }
}
